DD-WRT Dnscrypt-Proxy2 File Retrieval Over SSL
===

[[_TOC_]]

## Why

Upon installing and setting up Dnscrypt-Proxy2 in DD-WRT I was continually running into an issue where the required files would fail to download over SSL. I personally, do not like the thought of downloading Resolver and Relay lists via http. After reviewing logs and seeing no solution by installing any additional packages, I wrote this script and run it via Cron.

## How-To

1. Clone the Repo using git clone
2. Transfer `files.sh` to your Router using rsync or scp
3. Make `files.sh` executable - `chmod +x files.sh`
4. Setup Cron in web ui to automatically run the script every 3 days
    ex. `0 4 */3 * * root /path/to/files.sh`
5. Disable [sources] urls in `dnscrypt-proxy.toml`
    
    ex. Adjust 
        
    From:
    >`urls = ['https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md', 'https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md']`
        
    To:
    >`## urls = ['https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md', 'https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md']`
    
    Repeat for relays and disable `refresh_delay = 72`

## Extra Info

The original download/operating folder for the Dnscrypt-Proxy2 files is `/opt/etc`. I prefer having these files in a separate folder. `/opt/etc/config`

If you want the same configuration: 
1. `cd /opt/etc`
2. `mkdir config; mv dnscrypt-proxy.toml config/; mv blacklist.txt config/`
3. `vi init.d/S09dnscrypt-proxy2`
4. Adjust -
    
    From:
    >ARGS="-config /opt/etc/dnscrypt-proxy.toml"
    
    To:
    >ARGS="-config /opt/etc/config/dnscrypt-proxy.toml"

If you have your own configuration preference, be sure to edit line 3 of `files.sh` to reflect the appropiate path.

## Side-Notes

Verify `diff` is installed as you **_MAY_** need to install it using `opkg install diffutils`

It works on my device, soooo... :shrug: