#! /bin/sh

cd /opt/etc/config
mv public-resolvers.md public-resolvers.md.OLD
mv public-resolvers.minisig.md public-resolvers.md.minisig.OLD
mv relays.md relays.md.OLD
mv relays.md.minisig relays.md.minisig.OLD
if ! curl -s https://download.dnscrypt.info/dnscrypt-resolvers/v3/public-resolvers.md | diff - public-resolvers.md
then
    if ! curl -s https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md | diff - public-resolvers.md
    then
        curl -s https://download.dnscrypt.info/dnscrypt-resolvers/v3/public-resolvers.md > public-resolvers.md
        if [[ -s public-resolvers.md ]]
        then
            rm -rf public-resolvers.md.OLD
        else
            curl -s https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md > public-resolvers.md
            if [[ -s public-resolvers.md ]]
            then
                rm -rf public-resolvers.md.OLD
            else
                mv public-resolvers.md.OLD public-resolvers.md
                echo 'It appears both public-resolvers downloaded were empty...' >> empty-pr.txt
            fi
        fi
    fi
fi
if ! curl -s https://download.dnscrypt.info/dnscrypt-resolvers/v3/public-resolvers.md.minisig | diff - public-resolvers.md.minisig
then
    if ! curl -s https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md.minisig | diff - public-resolvers.md.minisig
    then
        curl -s https://download.dnscrypt.info/dnscrypt-resolvers/v3/public-resolvers.md.minisig > public-resolvers.md.minisig
        if [[ -s public-resolvers.md.minisig ]]
        then
            rm -rf public-resolvers.md.minisig.OLD
        else
            curl -s https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md.minisig > public-resolvers.md.minisig
            if [[ -s public-resolvers.md.minisig ]]
            then
                rm -rf public-resolvers.md.minisig.OLD
            else
                mv public-resolvers.md.minisig.OLD public-resolvers.md.minisig
                echo 'It appears both public-resolvers minisigs downloaded were empty...' >> empty-pr-mini.txt
            fi
        fi
    fi
fi
if ! curl -s https://download.dnscrypt.info/dnscrypt-resolvers/v3/relays.md | diff - relays.md
then
    if ! curl -s https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/relays.md | diff - relays.md
    then
        curl -s https://download.dnscrypt.info/dnscrypt-resolvers/v3/relays.md > relays.md
        if [[ -s relays.md ]]
        then
            rm -rf relays.md.OLD
        else
            curl -s https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/relays.md > relays.md
            if [[ -s relays.md ]]
            then
                rm -rf relays.md.OLD
            else
                mv relays.md.OLD relays.md
                echo 'It appears both relays downloaded were empty...' >> empty-relays.txt
            fi
        fi
    fi
fi
if ! curl -s https://download.dnscrypt.info/dnscrypt-resolvers/v3/relays.md.minisig | diff - relays.md.minisig
then
    if ! curl -s https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/relays.md.minisig | diff - relays.md.minisig
    then
        curl -s https://download.dnscrypt.info/dnscrypt-resolvers/v3/relays.md.minisig > relays.md.minisig
        if [[ -s relays.md.minisig ]]
        then
            rm -rf relays.minisig.OLD
        else
            curl -s https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/relays.md.minisig > relays.md.minisig
            if [[ -s relays.md.minisig ]]
            then
                rm -rf relays.md.minisig.OLD
            else
                mv relays.md.minisig.OLD relays.md.minisig
                echo 'It appears both relays minisigs downloaded were empty...' >> empty-relays-mini.txt
            fi
        fi
    fi
fi
